dlmaster:
  profiles:
    wireguard-server-do:
      provider: digitalocean
      image: 18.04.3 (LTS) x64
      size: s-2vcpu-4gb
      location: ams3
      script: custom-bootstrap-wireguard-server-salt
      script_args: -U -x python3 -F stable 2019.2.3
      minion:
        master: 192.168.11.2
    flesh-server-do:
      provider: digitalocean
      image: 18.04.3 (LTS) x64
      size: s-2vcpu-4gb
      location: ams3
      script: custom-bootstrap-flesh-server-salt
      script_args: -U -x python3 -F stable 2019.2.3
      minion:
        master: 192.168.11.2
    wireguard-server-gce:
      provider: gce
      image: ubuntu-1804-bionic-v20191113
      size: g1-small
      location: europe-west1-b
      script: custom-bootstrap-wireguard-server-salt
      script_args: -U -x python3 -F stable 2019.2.3
      minion:
        master: 192.168.11.2
    flesh-server-sw:
      provider: scaleway
      image: Ubuntu Bionic Beaver
      size: DEV1-S
      location: PAR 1
      script: custom-bootstrap-flesh-server-salt
      script_args: -U -x python3 -F stable 2019.2.3
      minion:
        master: 192.168.11.2