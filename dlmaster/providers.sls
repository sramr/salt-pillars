#!yaml|gpg
dlmaster:
  providers:
    digitalocean:
      personal_access_token: a45fa4309006bc391af80b570ddc621305a6262ab9c6f9169f0f6573f152505e
      ssh_key_file: /root/.ssh/id_rsa
      ssh_key_names: salt-master
      driver: digitalocean